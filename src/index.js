import types from "aktivitet/types";
import Page from "./page.js";

types.page = Page;

export { Page };
