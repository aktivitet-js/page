import globals from "aktivitet/globals";
import events from "aktivitet/events";
import selectors from "aktivitet/selectors";
import types from "aktivitet/types";
import init from "aktivitet/init";

selectors.PAGE_ROOT = '#page-root';
globals.$page_root = globals.$( selectors.PAGE_ROOT );

class Page extends types.parent
{
    load_url( url = null )
    {
        if ( url )
            this.options.url = url;

        var req = new XMLHttpRequest();
        req.addEventListener( 'load', function () {
            globals.$page_root.classList.remove( 'page-loading' )
            globals.$page_root.innerHTML = this.responseText;

            // run js code in page (could be danger)
            // src: https://stackoverflow.com/questions/2592092/executing-script-elements-inserted-with-innerhtml#answer-47614491
            Array.from( globals.$page_root.querySelectorAll( 'script' ) )
                .forEach( function ( old_s ) {
                    const new_s = document.createElement("script");

                    Array.from( old_s.attributes )
                        .forEach( function ( attr ) {
                            new_s.setAttribute( attr.name, attr.value )
                        } );
                        
                    new_s.appendChild( document.createTextNode( old_s.innerHTML ) );
                    old_s.parentNode.replaceChild(new_s, old_s);
                } );

            globals.$page_root.classList.add( 'page-loaded' )
            globals.$page_root.dispatchEvent( events.loaded() );
            init( selectors.PAGE_ROOT + ' ' + selectors.ACTIVITY )
        } );
        globals.$page_root.classList.add( 'page-loading' )
        req.open( 'GET', this.options.url );
        req.send();
    }

    handle_click( e )
    {
        if ( e.target.matches( 'a' ) )
        {
            this.load_url( e.target.attributes[ 'href' ].value );
            e.preventDefault();
        }
        else if ( e.target.matches( 'button' ) )
        {
            this.load_url();
        }
    }
}

Page.on( 'click', 'self', function ( e ) { this.handle_click( e ) } );

export default Page;
